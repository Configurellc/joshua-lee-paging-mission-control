import express from 'express';
import fs from 'fs';
import moment from 'moment';

const app = express();
const PORT = process.env.PORT || 3000;
const newLine = /\n/gi;
const lineSplit = /\r/gi;

app.set('view engine', 'ejs');
app.set('views', './views');

const inputFile = fs.readFileSync('input.txt', 'utf-8')
let inputData = inputFile.toString();
let numGroupCounter = inputData.split(/\r\n|\r|\n/).length;
        inputData = inputData.split('|');
        inputData = inputData.toString();
        inputData = inputData.replace(newLine, '');
        inputData = inputData.toString();
        inputData = inputData.replace(lineSplit, ',')
        inputData = inputData.toString()
        inputData = inputData.split(',')

function createGroups(arr, numGroups) {
    const perGroup = Math.ceil(arr.length / numGroups);
    const results = new Array(numGroups)
    return results
    .fill('')
    .map((_, i) => arr.slice(i * perGroup, (i + 1) * perGroup));
}

const teleArr = createGroups(inputData, numGroupCounter);
const sortTeleArr = teleArr.sort((a, b) => a - b);
const teleObj = sortTeleArr.map(([timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component]) => ({timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component}));

function groupBy(list, grabKey) {
    const map = new Map();
    list.forEach((item) => {
        const key = grabKey(item);
        const collection = map.get(key);
        
        if(!collection) {
            map.set(key, [item]);
        }else{
            collection.push(item);
        }
    });
    return map
}

const groupedSatellites = groupBy(teleObj, obj => obj.satelliteId);
const satelliteKey = [...groupedSatellites.keys()]; 
const satelliteValue = [...groupedSatellites.values()];
const lowVoltage = new Array();
const highTstat = new Array();
const tstatMinDate = moment(satelliteValue[0][0].timestamp, moment.defaultFormat).toDate();
const lowVoltMinDate = moment(satelliteValue[1][0].timestamp, moment.defaultFormat).toDate();

function getDiff(then, now ) {
    var dif = (then - now);
    var dif = Math.round((dif/1000)/60);
    return dif;
}

for (var i = 0; i < satelliteKey.length; i++) {
    if (groupedSatellites.has(satelliteKey[i])){
        for (var j = 0; j < satelliteValue[i].length; j++ ){
            let compareDate = moment(satelliteValue[i][j].timestamp, moment.defaultFormat).toDate();
            satelliteValue[i][j].timestamp = moment(satelliteValue[i][j].timestamp, moment.defaultFormat).toDate();
            if (satelliteValue[i][j].component === 'BATT' && Number(satelliteValue[i][j].rawValue) < Number(satelliteValue[i][j].redLowLimit) && getDiff(compareDate, lowVoltMinDate ) < 5 ) {
                satelliteValue[i][j].satelliteId = Number(satelliteValue[i][j].satelliteId);
                satelliteValue[i][j]['severity'] = 'RED LOW';
                let { redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, ...rest } = satelliteValue[i][j];
                lowVoltage.push(rest)
            }
            else if (satelliteValue[i][j].component === 'TSTAT' && Number(satelliteValue[i][j].rawValue) > Number(satelliteValue[i][j].redHighLimit) && getDiff(compareDate, tstatMinDate ) < 5 ) {
                satelliteValue[i][j].satelliteId = Number(satelliteValue[i][j].satelliteId);
                satelliteValue[i][j]['severity'] = 'RED HIGH';
                let { redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, ...rest } = satelliteValue[i][j];
                highTstat.push(rest);
            }           
        }        
    }
}

const orderedLowVoltage = JSON.parse(JSON.stringify(lowVoltage, ["satelliteId", "severity", "component", "timestamp"]));
const orderedHighTstat = JSON.parse(JSON.stringify(highTstat, ["satelliteId", "severity", "component", "timestamp"]));

app.get('/', (req, res) => { 
    res.render('index', {
        lowVoltage: JSON.stringify(orderedLowVoltage, null, '\t'),
        highTstat: JSON.stringify(orderedHighTstat, null, '\t'), 
        satelliteKey: satelliteKey.sort(),
    });
});

app.listen(PORT, () => {
    console.log(`Server Listening on port: ${PORT}` )
});